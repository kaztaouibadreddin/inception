#!/bin/sh

while ! mariadb -h $WP_DB_HOST -u$WP_DB_USR -p$WP_DB_PSWD $WP_DB_NAME &>/dev/null; do
    echo "waiting mariadb ....."
    sleep 3
done

wp core download --allow-root
wp config create --dbname=$WP_DB_NAME --dbuser=$WP_DB_USR --dbpass=$WP_DB_PSWD --dbhost=$WP_DB_HOST --dbcharset="utf8" --dbcollate="utf8_general_ci" --allow-root
wp core install --url=$DOMAIN_NAME --title=$WP_TITLE --admin_user=$WP_ADMIN_USR --admin_password=$WP_ADMIN_PSWD --admin_email=$WP_ADMIN_EMAIL --skip-email --allow-root
wp user create $WP_USR $WP_EMAIL --role=author --user_pass=$WP_PSWD --allow-root
wp theme install kubio --activate --allow-root

echo "Wordpress started on :9000"
exec php-fpm82 -F -R