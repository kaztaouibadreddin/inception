all:
	@mkdir -p /home/badreddin/data/wordpress
	@mkdir -p /home/badreddin/data/mariadb
	@docker-compose -f ./srcs/docker-compose.yml up
build:
	@docker-compose -f ./srcs/docker-compose.yml up  --build
status:
	@docker-compose -f ./srcs/docker-compose.yml ps
down:
	@docker-compose -f ./srcs/docker-compose.yml down
clean: down
	@docker system prune -af
fclean:
	@docker stop $$(docker ps -qa)
	@docker system prune -af
	@docker network prune --force
	@docker volume prune -f
	@sudo rm -rf /home/badreddin/data/wordpress
	@sudo rm -rf /home/badreddin/data/mariadb
re:	fclean all
	@docker-compose -f ./srcs/docker-compose.yml up  --build

.PHONY	: all build down re clean fclean
